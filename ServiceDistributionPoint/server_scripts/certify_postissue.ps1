$certname = 'hltlab.mooo.com'
$savepath = 'C:\Users\ne129\source\repos\ProjectManage\ServiceDistributionPoint\cert'
$passphrase = 'cc4511'

try {
	Write-Host "Certify ACME 클라이언트 자동 후처리 스크립트"
	Write-Output "Certify ACME 클라이언트 자동 후처리 스크립트"
	$cert = Get-ChildItem Cert:\LocalMachine\My | Where-Object {$_.Subject -match $certname }
	Write-Host "인증서 발견."
	Write-Host $cert[0]
	$pass = ConvertTo-SecureString -String $passphrase -Force -AsPlainText
	mkdir $env:TEMP\enpfx
	Write-Host "임시 폴더 생성."
	Export-PfxCertificate -Cert $cert[0] -FilePath $env:TEMP\enpfx\temp.pfx -Password $pass
	Write-Host "PFX 추출됨."
	cd $env:TEMP\enpfx
	Write-Host "임시 폴더 진입."
	openssl pkcs12 -in "temp.pfx" -nocerts -nodes -out "temp.key" -password pass:$passphrase
	Write-Host "개인키 추출(패스 프레이즈 제거)"
	openssl pkcs12 -in "temp.pfx" -clcerts -nokeys -out "temp.crt" -password pass:$passphrase
	Write-Host "인증서 추출(PKCS12 => X509 DER/BASE64 인코딩)"
	mv $env:TEMP\enpfx\temp.crt $savepath\acme-cert.crt -Force
	mv $env:TEMP\enpfx\temp.key $savepath\acme-nkey.key -Force
	cd ..
	Write-Host "상위 폴더"
	rmdir -Recurse $env:TEMP\enpfx
	Write-Host "임시 폴더 제거."

	Write-Output "인증서가 저장소로부터 추출되었습니다."
	Write-Output $cert[0]
}
catch {
	Write-Host "에러가 발생했습니다."
	Write-Output "에러가 발생했습니다."
}
$certname = 'hltlab.mooo.com'

try {
	Write-Host "Certify ACME 클라이언트 자동 전처리 스크립트"
	Write-Output "Certify ACME 클라이언트 자동 전처리 스크립트"
	$cert = Get-ChildItem Cert:\LocalMachine\My | Where-Object {$_.Subject -match $certname };
	if($cert.Count) {
        $cert | Remove-Item
	    Write-Output "오래된 인증서를 제거했습니다.";
    }
    else {
    	Write-Output "발급된 인증서가 없습니다.";
    }
}
catch {
	Write-Host "에러가 발생했습니다."
	Write-Output "에러가 발생했습니다."
}
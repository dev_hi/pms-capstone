-- 내 스키마
CREATE SCHEMA "current";

-- users
CREATE TABLE "current"."users"
(
	"user_id"     SERIAL      NOT NULL, -- 유저 ID키
	"username"    VARCHAR(64) NOT NULL, -- 유저로그인이름
	"userpass"    VARCHAR(64) NOT NULL, -- SHA256해쉬비번
	"last_access" timestamp   NOT NULL DEFAULT current_timestamp -- 마지막 로그인 시간
)
WITH (
OIDS=false
);

-- users 유니크 인덱스
CREATE UNIQUE INDEX "UIX_users"
	ON "current"."users"
	( -- users
		"username" ASC -- 유저로그인이름
	);

-- users 기본키
CREATE UNIQUE INDEX "PK_users"
	ON "current"."users"
	( -- users
		"user_id" ASC -- 유저 ID키
	)
;
-- users
ALTER TABLE "current"."users"
	ADD CONSTRAINT "PK_users"
		 -- users 기본키
	PRIMARY KEY 
	USING INDEX "PK_users";

-- users
ALTER TABLE "current"."users"
	ADD CONSTRAINT "UK_users" -- users 유니크 제약
	UNIQUE 
	USING INDEX "UIX_users";

-- projects
CREATE TABLE "current"."projects"
(
	"project_id"  SERIAL      NOT NULL, -- 프로젝트 ID키
	"title"       VARCHAR(64) NOT NULL, -- 프로젝트 이름
	"description" text        NOT NULL, -- 프로젝트 설명
	"leader"      SERIAL      NOT NULL  -- 팀장
)
WITH (
OIDS=false
);

-- projects 기본키
CREATE UNIQUE INDEX "PK_projects"
	ON "current"."projects"
	( -- projects
		"project_id" ASC -- 프로젝트 ID키
	)
;
-- projects
ALTER TABLE "current"."projects"
	ADD CONSTRAINT "PK_projects"
		 -- projects 기본키
	PRIMARY KEY 
	USING INDEX "PK_projects";

-- files
CREATE TABLE "current"."files"
(
	"file_id"     SERIAL       NOT NULL, -- 파일 ID키
	"filephy"     VARCHAR(64)  NOT NULL, -- 저장된 파일 이름
	"filesize"    INTEGER      NOT NULL DEFAULT 0, -- 파일 사이즈
	"filename"    VARCHAR(255) NOT NULL, -- 실제 파일 이름
	"upload_time" timestamp    NOT NULL DEFAULT current_timestamp, -- 업로드 시간
	"project_id"  SERIAL       NOT NULL, -- 프로젝트 ID키
	"prev_file"   SERIAL       NOT NULL, -- 이전 파일
	"user_id"     SERIAL       NOT NULL  -- 유저 ID키
)
WITH (
OIDS=false
);

-- files 기본키
CREATE UNIQUE INDEX "PK_files"
	ON "current"."files"
	( -- files
		"file_id" ASC -- 파일 ID키
	)
;
-- files
ALTER TABLE "current"."files"
	ADD CONSTRAINT "PK_files"
		 -- files 기본키
	PRIMARY KEY 
	USING INDEX "PK_files";

-- articles
CREATE TABLE "current"."articles"
(
	"article_id"          SERIAL      NOT NULL, -- article_id
	"title"               VARCHAR(64) NOT NULL, -- 제목
	"art_content"         text        NOT NULL, -- 내용
	"project_id"          SERIAL      NOT NULL, -- 프로젝트 ID키
	"user_id"             SERIAL      NOT NULL, -- 작성자
	"related_job_id"      SERIAL      NOT NULL, -- 연관 잡
	"related_schedule_id" SERIAL      NOT NULL, -- 연관 스케줄
	"related_file_id"     SERIAL      NOT NULL  -- 연관 파일
)
WITH (
OIDS=false
);

-- articles 기본키
CREATE UNIQUE INDEX "PK_articles"
	ON "current"."articles"
	( -- articles
		"article_id" ASC -- article_id
	)
;
-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "PK_articles"
		 -- articles 기본키
	PRIMARY KEY 
	USING INDEX "PK_articles";

-- schedules
CREATE TABLE "current"."schedules"
(
	"schedule_id"    SERIAL      NOT NULL, -- schedule_id
	"title"          VARCHAR(64) NOT NULL, -- 일정 제목
	"time_start"     timestamp   NOT NULL, -- 일정 시작
	"time_end"       timestamp   NOT NULL, -- 일정 끝
	"project_id"     SERIAL      NOT NULL, -- 프로젝트 ID키
	"related_job_id" SERIAL      NOT NULL  -- 연관 잡
)
WITH (
OIDS=false
);

-- schedules 기본키
CREATE UNIQUE INDEX "PK_schedules"
	ON "current"."schedules"
	( -- schedules
		"schedule_id" ASC -- schedule_id
	)
;
-- schedules
ALTER TABLE "current"."schedules"
	ADD CONSTRAINT "PK_schedules"
		 -- schedules 기본키
	PRIMARY KEY 
	USING INDEX "PK_schedules";

-- jobs
CREATE TABLE "current"."jobs"
(
	"job_id"              SERIAL      NOT NULL, -- job_id
	"assigned"            INTEGER[]   NULL,     -- 배정팀원
	"title"               VARCHAR(64) NOT NULL, -- 제목
	"job_content"         text        NOT NULL, -- 내용
	"project_id"          SERIAL      NOT NULL, -- 프로젝트 ID키
	"related_file_id"     SERIAL      NOT NULL, -- 파일 ID키
	"related_schedule_id" SERIAL      NOT NULL  -- schedule_id
)
WITH (
OIDS=false
);

-- jobs 기본키
CREATE UNIQUE INDEX "PK_jobs"
	ON "current"."jobs"
	( -- jobs
		"job_id" ASC -- job_id
	)
;
-- jobs
ALTER TABLE "current"."jobs"
	ADD CONSTRAINT "PK_jobs"
		 -- jobs 기본키
	PRIMARY KEY 
	USING INDEX "PK_jobs";

-- projects
ALTER TABLE "current"."projects"
	ADD CONSTRAINT "FK_users_TO_projects"
	 -- users -> projects
		FOREIGN KEY (
			"leader" -- 팀장
		)
		REFERENCES "current"."users" ( -- users
			"user_id" -- 유저 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- files
ALTER TABLE "current"."files"
	ADD CONSTRAINT "FK_projects_TO_files"
	 -- projects -> files
		FOREIGN KEY (
			"project_id" -- 프로젝트 ID키
		)
		REFERENCES "current"."projects" ( -- projects
			"project_id" -- 프로젝트 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- files
ALTER TABLE "current"."files"
	ADD CONSTRAINT "FK_files_TO_files"
	 -- files -> files
		FOREIGN KEY (
			"prev_file" -- 이전 파일
		)
		REFERENCES "current"."files" ( -- files
			"file_id" -- 파일 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- files
ALTER TABLE "current"."files"
	ADD CONSTRAINT "FK_users_TO_files"
	 -- users -> files
		FOREIGN KEY (
			"user_id" -- 유저 ID키
		)
		REFERENCES "current"."users" ( -- users
			"user_id" -- 유저 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "FK_projects_TO_articles"
	 -- projects -> articles
		FOREIGN KEY (
			"project_id" -- 프로젝트 ID키
		)
		REFERENCES "current"."projects" ( -- projects
			"project_id" -- 프로젝트 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "FK_users_TO_articles"
	 -- users -> articles
		FOREIGN KEY (
			"user_id" -- 작성자
		)
		REFERENCES "current"."users" ( -- users
			"user_id" -- 유저 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "FK_jobs_TO_articles"
	 -- jobs -> articles
		FOREIGN KEY (
			"related_job_id" -- 연관 잡
		)
		REFERENCES "current"."jobs" ( -- jobs
			"job_id" -- job_id
		)
		ON UPDATE SET NULL ON DELETE SET NULL
		NOT VALID;

-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "FK_schedules_TO_articles"
	 -- schedules -> articles
		FOREIGN KEY (
			"related_schedule_id" -- 연관 스케줄
		)
		REFERENCES "current"."schedules" ( -- schedules
			"schedule_id" -- schedule_id
		)
		ON UPDATE SET NULL ON DELETE SET NULL
		NOT VALID;

-- articles
ALTER TABLE "current"."articles"
	ADD CONSTRAINT "FK_files_TO_articles"
	 -- files -> articles
		FOREIGN KEY (
			"related_file_id" -- 연관 파일
		)
		REFERENCES "current"."files" ( -- files
			"file_id" -- 파일 ID키
		)
		ON UPDATE SET NULL ON DELETE SET NULL
		NOT VALID;

-- schedules
ALTER TABLE "current"."schedules"
	ADD CONSTRAINT "FK_projects_TO_schedules"
	 -- projects -> schedules
		FOREIGN KEY (
			"project_id" -- 프로젝트 ID키
		)
		REFERENCES "current"."projects" ( -- projects
			"project_id" -- 프로젝트 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- schedules
ALTER TABLE "current"."schedules"
	ADD CONSTRAINT "FK_jobs_TO_schedules"
	 -- jobs -> schedules
		FOREIGN KEY (
			"related_job_id" -- 연관 잡
		)
		REFERENCES "current"."jobs" ( -- jobs
			"job_id" -- job_id
		)
		ON UPDATE CASCADE ON DELETE SET NULL
		NOT VALID;

-- jobs
ALTER TABLE "current"."jobs"
	ADD CONSTRAINT "FK_projects_TO_jobs"
	 -- projects -> jobs
		FOREIGN KEY (
			"project_id" -- 프로젝트 ID키
		)
		REFERENCES "current"."projects" ( -- projects
			"project_id" -- 프로젝트 ID키
		)
		ON UPDATE CASCADE ON DELETE CASCADE
		NOT VALID;

-- jobs
ALTER TABLE "current"."jobs"
	ADD CONSTRAINT "FK_files_TO_jobs"
	 -- files -> jobs
		FOREIGN KEY (
			"related_file_id" -- 파일 ID키
		)
		REFERENCES "current"."files" ( -- files
			"file_id" -- 파일 ID키
		)
		ON UPDATE SET NULL ON DELETE SET NULL
		NOT VALID;

-- jobs
ALTER TABLE "current"."jobs"
	ADD CONSTRAINT "FK_schedules_TO_jobs"
	 -- schedules -> jobs
		FOREIGN KEY (
			"related_schedule_id" -- schedule_id
		)
		REFERENCES "current"."schedules" ( -- schedules
			"schedule_id" -- schedule_id
		)
		ON UPDATE SET NULL ON DELETE SET NULL
		NOT VALID;

ALTER table current.jobs add completed boolean default false;


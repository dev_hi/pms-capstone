﻿const Router = class {
    constructor(url, parent, cb) {
        this.URL = url;
        this.Parent = parent;
        this.Children = [];
        this.onHit = cb;
    }
    AppendTo(parent) {
        parent.Append(this);
        return this;
    }
    Append(child) {
        this.Children.push(child);
        child.Parent = this;
        return this;
    }
    GetNode(url) {
        var ret = this;
        var tmp = spliturl(url);
        if (tmp.length != 0) {
            ret = undefined;
            for (var child of this.Children) {
                if ((new RegExp(child.URL)).test(url) ? true : tmp[0] == child.URL) {
                    var suburi = "";
                    for (var i = 1; i < tmp.length; ++i)
                        suburi += "/" + tmp[i];
                    ret = child.GetNode(suburi);
                }
            }
        }
        return ret;
    }
    GetRoot() {
        return this.Parent == null ? this : this.GetRoot();
    }
    Route(uri) {
        var target = this.GetNode(uri);
        if (target) {
            if (GetSession() == null && uri != "/login" && uri != "/register") {
                CheckLogin();
            }
            else {
                var stack = [];
                var c = target;
                while (c) {
                    stack.push(c);
                    c = c.Parent;
                }
                for (var i = stack.length - 1; i >= 0; --i) {
                    if (stack[i].onHit)
                        if (stack[i].onHit(stack[i], uri, i == 0, (new RegExp(stack[i].GetAllUriPath()).exec(uri))))
                            break;
                }
            }
        }
        else {
            window.location = "/notfound";
        }
    }
    GetAllUriPath() {
        return (this.IsRoot() ? "" : this.Parent.GetAllUriPath() + (this.Parent.IsRoot() ? "" : "/")) + this.URL;
    }
    IsRoot() {
        return this.Parent == undefined;
    }
    CreateNode(uri, cb) {
        var ret;
        if (!this.IsExistNode(uri)) {
            var tmp = spliturl(uri);
            var self_uri = tmp[0];
            var suburi = "";
            for (var i = 1; i < tmp.length; ++i) suburi += "/" + tmp[i];
            if (self_uri == undefined) ret = this;
            else ret = (this.IsExistNode(self_uri) ? this.GetNode(self_uri) : (new Router(self_uri, this)).AppendTo(this)).CreateNode(suburi);
            ret.onHit = cb;
        }
        else ret = this.GetNode(uri);

        return ret;
    }
    IsExistNode(uri) {
        return this.GetNode(uri) != undefined;
    }
}

function spliturl(s) {
    return s.split('/').filter(function (el) {
        return el != undefined;
    }).filter(function (el) {
        return el != null;
    }).filter(function (el) {
        return el.length > 0;
    });
}

function GetSession() {
    return localStorage.getItem("API_SESSION");
}

function CheckLogin() {
    var sessionid = GetSession();
    if (sessionid == null) {
        pjax("/login");
        return false;
    }
    else {
        CheckSession().then(function (r) {
            if (r) {
                if (window.location.pathname == "/") pjax("/project");
                else if (window.location.pathname != "/project" && window.location.pathname != "/login") pjax(window.location.pathname);
            }
            else {
                pjax("/login");
            }
        }).catch(function (e) {
            console.error(e);
        });
    }

    return true;
}

function pjax(url) {
    history.pushState(undefined, undefined, url);
    RootRouter.Route(url);
}

function pjaxBack(url) {
    history.replaceState(undefined, undefined, url);
    RootRouter.Route(url);
}

function showPjax(j, a) {
    $(".pjax-holder").removeClass("pjax-active");
    $(j).addClass("pjax-active");
    if (a) $("body").addClass("screen-hold");
    else $("body").removeClass("screen-hold");
}

function showPjaxSub(j, m) {
    showPjax("#main_cont");
    $(".pjax-sub-holder").removeClass("pjax-sub-active");
    $(j).addClass("pjax-sub-active");
    if (m) {
        $(".nav-link").removeClass("bg-light");
        $(m).addClass("bg-light");
    }
}

var RootRouter = new Router("/", undefined, function (self, uri, t) {
    $("#project_menu").hide();
    if (t) pjax("/project");
});

RootRouter.CreateNode("/login", function (self, u, t) {
    showPjax("#signin", true);
});

RootRouter.CreateNode("/register", function (self, u, t) {
    showPjax("#signup", true);
});

RootRouter.CreateNode("/project", function (self, u, t) {
    if (t) {
        GetProjectList().then(function (r) {
            if (r) {
                console.log(r);
                $("#project_list").empty();
                for (var i = 0; i < r.length; ++i) {
                    $("#project_list").append('<a href="/project/' + r[i].project_id + '" class="list-group-item list-group-item-action">' + r[i].title +'</a>');
                }
                showPjaxSub("#projectsel", "#project_list_menu");
            }
        }).catch((e) => {
            alert('통신에러');
        });
    }
});

RootRouter.CreateNode("/project/([0-9]+)", function (s, u, t, r) {
    var projectid = r[1];
    $("#project_menu").show().attr("data-project-id", projectid);
    if (t) pjax("/project/" + projectid + "/dashboard");
});

RootRouter.CreateNode("/project/([0-9]+)/join", function (s, u, t, r) {
    $("#project_menu").show();
    var projectid = r[1];
    //TODO: JOIN API
});

RootRouter.CreateNode("/project/([0-9]+)/dashboard", function (s, u, t, r) {
    var projectid = r[1];
    GetProject(projectid).then(function (r2) {
        $("#project_title").html(r2.title);
        $("title").html("프로젝트 관리 : " + r2.title);
        $("#nav_role").html(r2.IsLeader ? "팀장" : "팀원");
        console.log(r2);
        showPjaxSub("#dashboard_scene", "#dashboard_menu");
    });
});

RootRouter.CreateNode("/project/([0-9]+)/member", function (s, u, t, r) {
    if (t) {
        GetMemeberList(r[1]).then(function (r2) {
            console.log(r2[0]);
            $("#member_list").empty();
            for (var i = 0; i < r2.length; ++i) {
                $('#member_list').append('<li class="list-group-item"><span class="membername">' + r2[i]["nickname"] + '</span><button class="btn btn-danger right" type="button">제거</button></li>');
            }
            showPjaxSub("#member_scene", "#member_menu");
        });
    }
});

RootRouter.CreateNode("/project/([0-9]+)/job", function (s, u, t, r) {
    if (t) showPjaxSub("#jobs_scene", "#jobs_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/contribution", function (s, u, t, r) {
    if (t) showPjaxSub("#contribution_scene", "#contribution_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/file", function (s, u, t, r) {
    if (t) showPjaxSub("#files_scene", "#files_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/schedule", function (s, u, t, r) {
    if (t) showPjaxSub("#schedule_scene", "#schedule_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/article", function (s, u, t, r) {
    if (t) showPjaxSub("#articlelist_scene", "#articles_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/article/([0-9]+)", function (s, u, t, r) {
    if (t) showPjaxSub("#showarticle_scene", "#articles_menu");
});

RootRouter.CreateNode("/project/([0-9]+)/article/write", function (s, u, t, r) {
    if (t) showPjaxSub("#writearticle_scene", "#articles_menu");
});

RootRouter.CreateNode("/project/create", function (s, u, t) {
    if (t) showPjaxSub("#newproject", "#project_list_menu");
});

$(function () {
    if (window.history && window.history.pushState) {
        $(window).on('popstate', function (e) {
            pjax(window.location.pathname);
        });
    }
    $('#datetimepicker2').datetimepicker({
        format: 'YYYY년 MM월 DD일',
        locale: 'ko-kr'
    });

    $('.btn-collapse-nav').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
     });

    $('#enter_register').on('click', function () {
        pjax("/register");
    });

    $('#perform_login').on('click', function () {
        Authenticate($("#inputUsername").val(), $("#inputPassword").val()).then(function (r) {
            if (r) {
                $("#wrongidpass").hide();
                console.log(r);
                $("#nav_nickname").html(r.nickname);
                pjax("/project");
            }
            else {
                $("#wrongidpass").show();
            }
        }).catch(function (e) {
            if (e.status == 401) {
                $("#wrongidpass").show();
            }
            else {
                console.error(e);
            }
        });
    });
    $('#perform_signup').on('click', function () {
        if ($("#inputUpPassword").val() == $("#inputUpPasswordConfirm").val()) {
            Register($("#inputUpUsername").val(), $("#inputUpPassword").val(), $("#inputUpNickname").val()).then(function (r) {
                if (r) {
                    $("#alreadyexists").hide();
                    pjax("/login");
                }
                else {
                    $("#alreadyexists").show();
                }
            }).catch(function (e) {
                if (e.status == 409) {
                    $("#alreadyexists").show();
                }
                else {
                    console.error(e);
                }
            });
        }
        else {
            alert("비밀번호를 확인해주세요.");
        }
    });

    $('#logout_menu').on('click', function () {
        SessionDestroy().then(function (r) {
            if (r) {
                alert("로그아웃 되었습니다.");
                pjax("/login");
            }
        }).catch(function (e) {
            alert("로그아웃 되었습니다.");
            pjax("/login");
        });
    });

    $('#btn_new_project').on('click', function () {
        pjax("/project/create");
    });

    $('#project_list_menu').on('click', function () {
        pjax("/project");
    });

    $('#dashboard_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/dashboard');
    });

    $('#member_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/member');
    });

    $('#jobs_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/job');
    });

    $('#articles_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/article');
    });

    $('#contribution_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/contribution');
    });

    $('#files_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/file');
    });

    $('#schedule_menu').on('click', function (e) {
        pjax('/project/' + $("#project_menu").attr("data-project-id") + '/schedule');
    });

    $('#perform_create_project').on('click', function (e) {
        CreateProject($("#inputProjectTitle").val(), $("#inputProjectDescription").val(), new Date($("#datetimepicker2").data("datetimepicker")._datesFormatted[0]).getTime()).then(function (r) {
            pjax('/project/' + r.project_id);
        });
    });

    $('#user_join_btn').on('click', function (e) {
        var project_id = parseInt($("#project_menu").attr("data-project-id"));
        var username = $('#jointouserfield').val();
        JoinUserToProject(project_id, username).then(function (r) {
            alert("추가되었습니다.");
            pjax('/project/' + project_id + '/member');
        });
    });
        
    pjax(window.location.pathname);
});

﻿async function GET(url) {
    return await $.ajax({
        method: "GET",
        url: url,
        beforeSend: function (xhr) {
            var api_session = localStorage.getItem("API_SESSION");
            if (api_session != null) xhr.setRequestHeader("API-Session", api_session);
        }
    });
}

async function POST(url, data) {
    return await $.ajax({
        method: "POST",
        url: url,
        beforeSend: function (xhr) {
            var api_session = localStorage.getItem("API_SESSION");
            if (api_session != null) xhr.setRequestHeader("API-Session", api_session);
        },
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data)
    });
}

async function Authenticate(username, password) {
    var t = await POST("/api/auth", {
        username: username,
        password: password
    });
    if (t.session) {
        /*
        message: "authenticated successful.",
        session: sid,
        username: r[0]["username"],
        user_id: r[0]["user_id"],
        nickname: r[0]["nickname"]
         */
        localStorage.setItem("API_SESSION", t.session);
        localStorage.setItem("USERNAME", t.username);
        localStorage.setItem("USER_ID", t.user_id);

        return t;
    }
    else {
        return undefined;
    }
}

async function Register(username, password, nickname) {
    var t = await POST("/api/register", {
        username: username,
        password: password,
        nickname: nickname
    });
    if (t) {
        return true;
    }
    else {
        return false;
    }
}

async function CheckSession() {
    var t = await GET('/api/session/check');
    if (t.code == 0) {
        return true;
    }
    else {
        localStorage.clear();
        return false;
    }
}

async function SessionDestroy() {
    localStorage.clear();
    await POST('/api/session/destroy');
    if (!await CheckSession()) {
        return true;
    }
    else {
        return false;
    }
}

async function CreateProject(title, desc, expire) {
    return await POST('/api/project/create', {
        title: title,
        desc: desc,
        expire: expire
    });
}

async function GetProjectList() {
    return await GET('/api/project/list');
}

async function GetProject(project_id) {
    return await GET('/api/project/' + project_id);
}

async function JoinUserToProject(project_id, username) {
    console.log(username);
    return await POST('/api/project/' + project_id + '/join', {
        username: username
    });
}

async function GetMemeberList(project_id) {
    return await GET('/api/project/' + project_id + '/members');
}
﻿'use strict';

const multer = require('multer');
const sessions = require('./session');
const dbaccess = require('./pghelper');
const express = require('express');
const router = express.Router();

const upload_handle = multer({ dest: 'files/', limits: { fileSize: 100 * 1024 * 1024 } });

router.get('/', function (req, res) {
    res.status(404).json({
        message: "no endpoint given"
    });
});

router.get('^/project/:pid([0-9]+)/file/:fid([0-9]+)', function (req, res) {
    console.log(req.url);
});

router.post('^/project/:pid([0-9]+)/file/:fid([0-9]+)', upload_handle.single('f'), function (req, res) {
    console.log(req.url);
});

router.post('^/project/:pid([0-9]+)/file', upload_handle.single('f'), function (req, res) {
    console.log(req.url);
});

router.get("^/project/:pid([0-9]+)/", function (req, res) {
    dbaccess.GetProject(req.params.pid).then(function (r) {
        if (r) {
            if (r.length > 0) {
                var ret = r[0];
                ret.IsLeader = r[0]["leader"] == req.session.user_id;
                res.status(200).json(ret);
            }
            else {
                res.status(404).json({
                    message: "not found"
                });
            }
        }
        else {
            res.status(404).json({
                message: "not found"
            });
        }
    }).catch((e) => {
        res.status(500).json({
            message: "error"
        });
    });
});

router.post('/project/create', function (req, res) {
    dbaccess.NewProject(req.session.user_id, req.body.title, req.body.desc, new Date((req.body.expire))).then(function (r) {
        if (r) {
            res.status(200).json({
                project_id: r[0]["project_id"],
                title: r[0]["title"]
            });
        }
        else {
            res.status(500).json({
                message: "an error has been occured."
            });
        }
    }).catch(function (e) {
        console.log(e);
        res.status(500).json({
            message: "an error has been occured."
        });
    });
});

router.get('/project/list', function (req, res) {
    dbaccess.GetProjects(req.session.user_id).then(function (r) {
        if (r) {
            res.status(200).json(r);
        }
        else {
            res.status(500).json({
                message: "an error has been occured."
            });
        }
    }).catch(function (e) {
        console.log(e);
        res.status(500).json({
            message: "an error has been occured."
        });
    });
});

router.post('^/project/:pid([0-9]+)/delete', function (req, res) {

});


router.get('^/project/:pid([0-9]+)/members', function (req, res) {
    dbaccess.GetMembers(req.params.pid).then(function (r) {
        if (r) {
            res.status(200).json(r);
        }
    });

});

router.post('^/project/:pid([0-9]+)/join', function (req, res) {
    console.log(req.body.username);
    dbaccess.JoinUser(req.params.pid, req.body.username).then(function (r) {
        if (r) {
            res.status(200).json({
                message: "user joined."
            });
        }
    }).catch(function (e) {
        console.log(e);
        res.status(500).json({
            message: "an error has been occured."
        });
    });
});

router.post('^/project/:pid([0-9]+)/leave', function (req, res) {

});

router.get('^/project/:pid([0-9]+)/article/:artid([0-9]+)', function (req, res) {

});

router.get('^/project/:pid([0-9]+)/article/:artid([0-9]+)/delete', function (req, res) {

});

router.post('^/project/:pid([0-9]+)/article/write', function (req, res) {

});

router.get('^/project/:pid([0-9]+)/article/list', function (req, res) {

});

router.get('^/project/:pid([0-9]+)/schedule/list', function (req, res) {

});

router.get('^/project/:pid([0-9]+)/schedule/:scid([0-9]+)', function (req, res) {

});

router.post('^/project/:pid([0-9]+)/schedule/create', function (req, res) {

});

router.post('/auth', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    console.log(req.body);

    dbaccess.CheckPass(username, password).then(function (r) {
        if (r == undefined) {
            res.status(401).json({
                message: "authenticate failed."
            })
        }
        else {
            var sid = sessions.issueSession(username, (new Date().getTime()));
            sessions.addSession(sid, r[0]["user_id"]);
            res.status(200);
            res.setHeader("API-Session", sid);
            console.log(r);
            res.json({
                message: "authenticated successful.",
                session: sid,
                username: r[0]["username"],
                user_id: r[0]["user_id"],
                nickname: r[0]["nickname"]
            });
        }
    }).catch(function (e) {
        console.log(e);
        res.status(500).json({
            message: "an error has been occured."
        });
    });
});

router.post('/register', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var nickname = req.body.nickname;

    console.log(req.body);

    dbaccess.NewUser(username, password, nickname).then(function (r) {
        if (r) {
            res.status(200).json({
                message: "registered successful."
            });
        }
        else {
            res.status(409).json({
                message: "username already registered."
            });
        }
    }).catch(function (e) {
        console.log(e);
        res.status(500).json({
            message: "an error has been occured."
        });
    });

});

router.post('/session/destroy', function (req, res) {

    if (req.session) {
        sessions.remSession(req.session.sid);
        if (sessions.getSession(req.session.sid) == undefined) {
            res.status(200).json({
                message: "session destroyed successful."
            });
        }
        else {
            res.status(500).json({
                message: "session not destroyed."
            });
        }
    }
    else {
        res.status(403).json({
            message: "no given session."
        });
    }
});

router.use(function (req, res) {
    res.status(404).json({
        message: "endpoint not found."
    });
});

module.exports = router;

﻿Sessions = {}

//MurmurHash3 자바스크립트 포팅
//https://en.wikipedia.org/wiki/MurmurHash

function xmur3(a) {
    for (var i = 0, seed = 1779033703 ^ a.length; i < a.length; i++)
        seed = Math.imul(seed ^ a[i], 3432918353),
            seed = seed << 13 | seed >>> 19;
    return function () {
        seed = Math.imul(seed ^ seed >>> 16, 2246822507);
        seed = Math.imul(seed ^ seed >>> 13, 3266489909);
        return (seed ^= seed >>> 16) >>> 0;
    }
}

function issueSession(str, n) {
    var isalt = 0;
    for (var i = 0; i < str.Length - 2; ++i) {
        isalt <<= 3;
        isalt |= (str[i + 1] & 0xA8 % str[i] ^ 0xFF << str[i + 2] & (str[i + 1] ^ 0x1A));
    }
    var seed = n;
    for (var i = 0; i < 4; ++i) {
        seed ^= n ^ (0xA8 ^ isalt % (str[0] ^ isalt) & (n ^ isalt << 0xF));
        if (i != 3) seed <<= 8;
    }

    var rand = xmur3([seed, n, isalt]);
    var session = 0;
    for (var i = 0; i < 8; ++i) {
        session |= rand() % 0xFF;
        if (i != 7) session <<= 8;
    }

    return (session >>> 0).toString(16);
}

function AddSession(sid, val) {
    Sessions[sid] = val;
}

function RemSession(sid) {
    delete Sessions[sid];
}

function GetSession(sid) {
    return Sessions[sid];
}

module.exports = {
    issueSession: issueSession,
    sessions: Sessions,
    addSession: AddSession,
    remSession: RemSession,
    getSession: GetSession
}
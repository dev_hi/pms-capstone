﻿
const crypto = require('crypto');
const pg = require('pg');
const { Pool } = require('pg');
const fs = require('fs');

const settings = JSON.parse(fs.readFileSync("./setting.json"));
const pool = new Pool(settings.postgresql);
module.exports = {
    GetUser: async function (user_id) {
        let data = await pool.query("select * from current.users where user_id = $1", [user_id]);
        return data;
    },
    CheckPass: async function (username, password) {
        var pwsha256 = crypto.createHash("sha256").update(password + username).digest('hex');
        let data = await pool.query("select * from current.users where username = $1 and userpass = $2", [username, pwsha256]);
        if (data.rowCount > 0) {
            return data.rows;
        }
        else {
            return undefined;
        }
    },
    UpdateAccess: async function (user_id) {
        try {
            await pool.query("update current.users set last_access = current_timestamp where user_id = $1", [user_id]);
            return true;
        }
        catch (e) {
            return false;
        }
    },
    NewUser: async function (username, password, nickname) {
        try {
            var pwsha256 = crypto.createHash("sha256").update(password + username).digest('hex');
            await pool.query("insert into current.users (username, userpass, nickname) values ($1, $2, $3)", [username, pwsha256, nickname]);
            return true;
        }
        catch (e) {
            console.log(e);
            return false;
        }
    },
    GetProjects: async function (user_id) {
        let data = await pool.query("select * from current.projects where leader = $1 or $1 = any (users)", [user_id]);
        return data.rows;
    },
    GetProject: async function (project_id) {
        let data = await pool.query("select * from current.projects where project_id = $1", [project_id]);
        if (data.rowCount > 0) {
            return data.rows;
        }
        else {
            return undefined;
        }
    },
    NewProject: async function (leader, title, desc, expire) {
        try {
            await pool.query("insert into current.projects (title, description, leader, deadline) values ($1, $2, $3, $4)", [title, desc, leader, expire]);
            var data = await pool.query("select * from current.projects order by project_id desc limit 1");
            if (data.rowCount > 0) {
                return data.rows;
            }
            else {
                return undefined;
            }
        }
        catch (e) {
            console.log(e);
            return false;
        }
    },
    DeleteProject: async function (project_id) {

    },
    JoinUser: async function (project_id, username) {
        try {
            console.log(username);
            await pool.query("update current.projects set users = array_append(users, (select user_id from current.users where username = $2)) where project_id = $1", [project_id, username]);
            return true;
        }
        catch (e) {
            console.log(e);
            return false;
        }
    },
    GetJobs: async function (project_id, user_id) {
        let data = await pool.query("select * from current.jobs where project_id = $1 " + (user_id ? "and user_id = $2" : ""), [project_id, user_id]);
        if (data.rowCount > 0) {
            return data.rows;
        }
        else {
            return undefined;
        }
    },
    GetJob: async function (job_id) {
        let data = await pool.query("select * from current.jobs where job_id = $1", [job_id]);
        if (data.rowCount > 0) {
            return data.rows;
        }
        else {
            return undefined;
        }
    },
    GetArticle: async function (article_id) {

    },
    GetArticles: async function (project_id) {

    },
    NewArticle: async function (title) {

    },
    GetSchedules: async function (project_id) {

    },
    GetSchedule: async function (schedule_id) {

    },
    NewSchedule: async function (project_id, time_start, time_end) {

    },
    GetFiles: async function (project_id) {

    },
    GetFile: async function (file_id) {

    },
    AddFile: async function () {

    },
    AddFileDelta: async function (file_id) {

    },
    RevertFileDelta: async function (file_id, delta_id) {

    },
    GetMembers: async function (project_id) {
        let data = await pool.query("select * from current.users where (select users from current.projects where project_id = $1) @> ARRAY[user_id] or user_id = (select leader from current.projects where project_id = $1)", [project_id]);
        if (data.rowCount > 0) {
            return data.rows;
        }
        else {
            return undefined;
        }
    }
};
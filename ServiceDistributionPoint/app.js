﻿'use strict';
var debug = require('debug');
var express = require('express');
var fs = require('fs');
var path = require('path');
var logger = require('morgan');
var https = require('https');
var redis = require('redis');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sessions = require('./submodules/session');
var users = require('./submodules/api');

var app_secred = express();
var app = express();

app_secred.use(function (req, res, next) {
    res.status(307).redirect("https://" + req.hostname + req.originalUrl);
});

app.get('/api/session/check', function (req, res, next) {
    if (req.headers["api-session"] != undefined) {
        var uid = sessions.getSession(req.headers["api-session"]);
        if (uid != undefined) {
            res.status(200).json({
                code: 0,
                message: 'session alive'
            });
        }
        else {
            res.status(200).json({
                code: 1,
                message: 'session not found'
            });
        }
    }
    else {
        next();
    }
});

app.use(function (req, res, next) {
    if (req.headers["api-session"] != undefined) {
        var uid = sessions.getSession(req.headers["api-session"]);
        if (uid != undefined) {
            req.session = {
                sid: req.headers["api-session"],
                user_id: uid
            }
        }
        else {
            throw { message: 'session expired.', status: 401 };
        }
    }
    next();
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/api', users);
app.use('/notfound', function (req, res) {
    res.status(404).render('notfound', { title: 'Project Managenment for Students' });
});

app.use(function (req, res) {
    res.render('index', { title: 'Project Managenment for Students' });
});

if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500).json({
            message: err.message,
            error: err
        });
    });
}
else {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500).json({
            error: {},
            message: err.message
        });
    });
}

app_secred.set('port', process.env.PORT || 80);
app.set('port', process.env.PORT || 443);

var https_redirect_server = app_secred.listen(app_secred.get('port'), function () {
    debug('Express server listening on port ' + https_redirect_server.address().port);
});


var server_https = https.createServer({ key: fs.readFileSync("./cert/acme-nkey.key"), cert: fs.readFileSync("./cert/acme-cert.crt") }, app);
server_https.listen(app.get('port'));
